package com.kosobudzki.customfontcanvas;

import android.text.TextPaint;
import android.text.TextUtils;
import android.util.SparseArray;

import java.util.ArrayList;
import java.util.List;

/**
 * Helper class for drawing ellipsis text on canvas.
 *
 * @author krzysztof.kosobudzki
 */
public class CanvasEllipsisTextUtil {
    private static final String sEllipsis = "...";

    private final TextPaint mTextPaint;
    private final String mText;
    private final int mWidth;
    private final int mLines;

    private final int mTextLength;

    private int mMaxCharacters = -1;

    private CanvasEllipsisTextUtil(final Builder builder) {
        mTextPaint = builder.mTextPaint;
        mText = builder.mText;
        mWidth = builder.mWidth;
        mLines = builder.mLines;

        mTextLength = mText.length();
    }

    /**
     * Returns max number of characters which can be drawn on canvas.
     *
     * Methods will calculate number of the characters only once and this number is cached.
     *
     * @return
     */
    public int maxCharacters() {
        if (mMaxCharacters >= 0) {
            return mMaxCharacters;
        }

        mMaxCharacters = mTextLength;

        if (mTextPaint.measureText(mText) <= mWidth) {
            return mMaxCharacters;
        }

        if (TextUtils.isEmpty(mText)) {
            mMaxCharacters = 0;

            return mMaxCharacters;
        }

        final String[] words = cuttedWords(mText.split(" "));

        float currentLineWidth;
        int lineNum = 0;

        SparseArray<String> lines = new SparseArray<String>();

        currentLineWidth = mTextPaint.measureText(words[0]);
        lines.put(0, words[0]);

        if (words.length == 1) {
            mMaxCharacters = lines.get(0).length();

            return mMaxCharacters;
        }

        final String[] wordsWithoutFirst = new String[words.length - 1];

        System.arraycopy(words, 1, wordsWithoutFirst, 0, words.length - 1);

        for (String word : wordsWithoutFirst) {
            if (lineNum > mLines) {
                break;
            }

            if (currentLineWidth + mTextPaint.measureText(word) < mWidth) {
                // remember about space
                currentLineWidth += mTextPaint.measureText(String.format("%s ", word));

                lines.put(lineNum, String.format("%s %s", lines.get(lineNum), word));
            } else {
                currentLineWidth = mTextPaint.measureText(word);

                lines.put(++lineNum, word);
            }
        }

        mMaxCharacters = sumLength(lines, mLines);

        return mMaxCharacters;
    }

    /**
     * Sum the lenght of all strings inside {@param lines}.
     *
     * If {@param lines} size is more than {@param maxIndex} then only lenght up to {@param maxIndex}
     * is returned.
     *
     * @param lines
     * @param maxIndex
     * @throws IllegalArgumentException when {@param lines} is null
     * @return
     */
    private int sumLength(final SparseArray<String> lines, final int maxIndex) {
        if (lines == null) {
            throw new IllegalArgumentException("Array can not be null.");
        }

        int internalMaxIndex = maxIndex;

        if (maxIndex == 0) {
            return 0;
        }

        if (internalMaxIndex > lines.size()) {
            internalMaxIndex = lines.size();
        }

        int count = 0;

        for (int i = 0; i < internalMaxIndex; i++) {
            count += lines.get(i).length();
        }

        // new line loose a space char
        count += internalMaxIndex - 1;

        return count;
    }

    /**
     * Cuts words when they are wider than one line.
     *
     * @param words
     * @return
     */
    private String[] cuttedWords(final String[] words) {
        final List<String> cuttedWordsList = new ArrayList<String>();

        for (String word : words) {
            if (word != null) {
                if (mTextPaint.measureText(word) > mWidth) {
                    // cut
                    int totalWordWidth = 0;
                    int maxLenght = word.length();

                    for (int i = 0; i < Math.ceil(mTextPaint.measureText(word) / mWidth); i++) {
                        int wordEnd = totalWordWidth;

                        while (mTextPaint.measureText(mText, totalWordWidth, wordEnd) < mWidth && wordEnd < maxLenght) {
                            wordEnd++;
                        }

                        cuttedWordsList.add(word.substring(totalWordWidth, wordEnd) + "");

                        totalWordWidth = wordEnd;
                    }
                } else {
                    cuttedWordsList.add(word);
                }
            }
        }

        return cuttedWordsList.toArray(new String[cuttedWordsList.size()]);
    }

    /**
     * Returns cutted text with ellipsis.
     *
     * When text fits canvas without cut then ellipsis will not be returned.
     *
     * @return
     */
    public CharSequence cutToFit() {
        if (maxCharacters() == mTextLength) {
            return mText;
        }

        // take care of substring problem in Java 6.
        return String.format("%s%s", mText.substring(0, maxCharacters()) + "", sEllipsis);
    }

    /**
     * Set text paint.
     *
     * Creates {@link CanvasEllipsisTextUtil.Builder} object.
     *
     * @param textPaint
     * @throws IllegalArgumentException when {@param textPaint} is null
     * @return
     */
    public static final Builder on(final TextPaint textPaint) {
        if (textPaint == null) {
            throw new IllegalArgumentException("Can not draw on empty paint.");
        }

        return new Builder(textPaint);
    }

    public static class Builder {
        private final TextPaint mTextPaint;
        private String mText;
        private int mWidth;
        private int mLines;

        private Builder(final TextPaint textPaint) {
            mTextPaint = textPaint;
        }

        /**
         * Set text to draw.
         *
         * @param text
         * @throws IllegalArgumentException when {@param text} is null
         * @return
         */
        public CanvasEllipsisTextUtil write(final String text) {
            if (text == null) {
                throw new IllegalArgumentException("Can not draw null text");
            }

            mText = text;

            return new CanvasEllipsisTextUtil(this);
        }

        /**
         * Set width of the text.
         *
         * @param width
         * @throws IllegalArgumentException when {@param width} is less or equal 0
         * @return
         */
        public Builder width(final int width) {
            if (width <= 0) {
                throw new IllegalArgumentException("Can not draw on empty width");
            }

            mWidth = width;

            return this;
        }

        /**
         * Set max lines of the text.
         *
         * @param maxLines
         * @throws IllegalArgumentException when {@param maxLines} is less or equal 0
         * @return
         */
        public Builder max(final int maxLines) {
            if (maxLines <= 0) {
                throw new IllegalArgumentException("Can not draw less then one line");
            }

            mLines = maxLines;

            return this;
        }
    }
}
