package com.kosobudzki.customfontcanvas;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;

/**
 * Cache for typeface.
 *
 * Fetching typeface each time is both time and memory consuming.
 *
 * @author krzysztof.kosobudzki
 */
public class TypefaceCache {
    public static final String DEFAULT_FONT = "font/TimesNewRoman.ttf";

    private static TypefaceCache sObj;

    private final Typeface mTypeface;

    private TypefaceCache(final AssetManager assetManager, final String fontFileName) {
        mTypeface = Typeface.createFromAsset(assetManager, fontFileName);
    }

    public static final TypefaceCache getInstance(final Context context) {
        return getInstance(context, DEFAULT_FONT);
    }

    public static final TypefaceCache getInstance(final Context context, final String fontFileName) {
        if (sObj == null) {
            sObj = new TypefaceCache(context.getAssets(), fontFileName);
        }

        return sObj;
    }

    public Typeface getTypeface() {
        return mTypeface;
    }
}
