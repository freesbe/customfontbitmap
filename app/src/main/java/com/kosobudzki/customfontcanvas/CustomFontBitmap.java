package com.kosobudzki.customfontcanvas;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.TextUtils;
import android.widget.RemoteViews;

import java.lang.ref.WeakReference;

import rx.Observable;
import rx.Subscriber;
import rx.functions.Action1;

/**
 * Class generating bitmap with custom font.
 *
 * The main purpose to use this class is {@link android.widget.TextView} inside {@link android.widget.RemoteViews}
 * doesn't support changing font in another way.
 *
 * Class will generate bitmap with supplied text and custom font. Next, this bitmap should
 * be attached to {@link android.widget.ImageView}.
 *
 * @author krzysztof.kosobudzki
 */
public class CustomFontBitmap {
    private final WeakReference<Context> mContextReference;

    private final Typeface mTypeface;

    private int mWidth;
    private int mFontSize;
    private int mColor;
    private int mLines;
    private boolean mBold;
    private String mText;
    private int mWidgetId;

    public CustomFontBitmap(final Builder builder) {
        mContextReference = new WeakReference<Context>(builder.mContext);

        if (TextUtils.isEmpty(builder.mFontFileName)) {
            mTypeface = TypefaceCache.getInstance(builder.mContext).getTypeface();
        } else {
            mTypeface = TypefaceCache.getInstance(builder.mContext, builder.mFontFileName).getTypeface();
        }

        mWidth = builder.mWidth;
        mFontSize = builder.mContext.getResources().getDimensionPixelSize(builder.mFontSize);
        mColor = builder.mContext.getResources().getColor(builder.mColor);
        mLines = builder.mLines;
        mText = builder.mText;
        mWidgetId = builder.mWidgetId;
        mBold = builder.mBold;
    }

    /**
     * Returns builder for class on {@param context}.
     *
     * @param context
     * @return
     */
    public static final Builder with(final Context context) {
        return new Builder(context);
    }

    /**
     * Set generated text on {@param imageViewId}.
     *
     * @param remoteViews
     * @param imageViewId
     */
    public void on(final RemoteViews remoteViews, final int imageViewId) {
        getBitmapObservable()
                .compose(RxJavaUtils.<Bitmap>applySchedulers())
                .subscribe(new Action1<Bitmap>() {
                    @Override
                    public void call(Bitmap bitmap) {
                        final Context context = mContextReference.get();

                        if (context != null) {
                            remoteViews.setBitmap(imageViewId, "setImageBitmap", bitmap);

                            AppWidgetManager.getInstance(context)
                                    .updateAppWidget(mWidgetId, remoteViews);
                        }
                    }
                });
    }

    /**
     * Returns observable from bitmap.
     *
     * @return
     */
    private final Observable<Bitmap> getBitmapObservable() {
        return Observable.create(new Observable.OnSubscribe<Bitmap>() {
            @Override
            public void call(Subscriber<? super Bitmap> subscriber) {
                if (subscriber.isUnsubscribed()){
                    subscriber.onCompleted();

                    return;
                }

                subscriber.onNext(generateBitmap());
                subscriber.onCompleted();
            }
        });
    }

    /**
     * Generates bitmap with supplied text.
     *
     * @return
     */
    private final Bitmap generateBitmap() {
        final Context context = mContextReference.get();

        if (context == null) {
            throw new IllegalStateException("Context is null!");
        }

        final TextPaint paint = new TextPaint();
        paint.setAntiAlias(true);
        paint.setTypeface(mTypeface);
        paint.setColor(mColor);
        paint.setTextSize(mFontSize);
        paint.setFakeBoldText(mBold);

        final Bitmap bitmap = Bitmap.createBitmap(mWidth, (int) (1.15f * mLines * mFontSize), Bitmap.Config.ARGB_4444);
        final Canvas canvas = new Canvas(bitmap);

        final CanvasEllipsisTextUtil canvasTextUtil = CanvasEllipsisTextUtil.on(paint)
                .max(mLines)
                .width(mWidth)
                .write(mText);

        final StaticLayout staticLayout = new StaticLayout(
                canvasTextUtil.cutToFit(), paint, mWidth, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, true
        );

        staticLayout.draw(canvas);

        return bitmap;
    }

    /**
     * Builder class.
     */
    public static class Builder {
        private Context mContext;
        private String mFontFileName;
        private int mWidth;
        private int mFontSize;
        private int mColor;
        private int mLines;
        private boolean mBold;
        private String mText;
        private int mWidgetId;

        public Builder(final Context context) {
            mContext = context;
        }

        /**
         * Set widgetId.
         *
         * @param widgetId
         * @return
         */
        public Builder forWidget(final int widgetId) {
            mWidgetId = widgetId;

            return this;
        }

        /**
         * Set path to the font file within {@code assets} folder.
         *
         * @param fontFileName
         * @return
         */
        public Builder use(final String fontFileName) {
            mFontFileName = fontFileName;

            return this;
        }

        /**
         * Set width of the component in pixels.
         *
         * @param width
         * @return
         */
        public Builder width(final int width) {
            mWidth = width;

            return this;
        }

        /**
         * Set size of the font.
         *
         * This should be id of the dimmension, not the size itself.
         *
         * @param fontSize
         * @return
         */
        public Builder size(final int fontSize) {
            mFontSize = fontSize;

            return this;
        }

        /**
         * Set font to bold.
         *
         * @return
         */
        public Builder reallyMatters() {
            mBold = true;

            return this;
        }

        /**
         * Set color of the font.
         *
         * This should be id of the color, not the color itself.
         *
         * @param color
         * @return
         */
        public Builder color(final int color) {
            mColor = color;

            return this;
        }

        /**
         * Set number of lines.
         *
         * @param lines
         * @return
         */
        public Builder lines(final int lines) {
            mLines = lines;

            return this;
        }

        /**
         * Set text to write.
         *
         * @param text
         * @return
         */
        public CustomFontBitmap write(final String text) {
            mText = text;

            return new CustomFontBitmap(this);
        }
    }
}
