Custom Font Bitmap
==================
Generates text with custom font on bitmap. Should be used for Android widgets.

Usage
-----
Library needs `RxJava` to work properly.

```
CustomFontUtil.with(context)
                    .forWidget(widgetId)
                    .width(textContainerWidth)
                    .size(R.dimen.font_size)
                    .color(R.color.font_color)
                    .reallyMatters()
                    .lines(numberOfLines)
                    .write(textToWrite)
                    .on(remoteViews, R.id.title_container);
```

Configuration methods:
----------------------

+ `forWidget(widgetId)` set widget id whend the text should be displayed
+ `width(textContainerWidth)` set width of text container
+ `size(R.dimen.font_size)` set font size
+ `color(R.color.font_color)` set font color
+ `reallyMatters()` make text bold
+ `lines(numberOfLines)` maximum number of lines. When text is longer then `numberOfLines` then it will be cutted with ellipsis. Shorter text will be displayed with empty space below.
+ `write(textToWrite)` set text to write
+ `on(remoteViews, R.id.title_container)` set remoteViews and id of component where text should appear.

Ellipsis text
=============
Project contains util class wich is able to cut text to fit the canvas and ellipsis it. For this purpose use `CanvasEllipsisTextUtil`.

Fitting text
------------
Class honor words, so text will not be cutted on them. If word (or words) are longer than one line then they will be cutted to fit the line width.

Usage
-----
```
final CanvasEllipsisTextUtil canvasTextUtil = CanvasEllipsisTextUtil.on(textPaint)
                .max(maxLines)
                .width(textWidth)
                .write(textToWrite);
                
final CharSequence fitText = canvasTextUtil.cutToFit();
```

Configuration methods:
----------------------

+ `on(textPaint)` set `TextPaint` with custom font
+ `max(maxLines)` set maximum number of lines. If text exeeds `maxLines` then it's cutted. When text fits lines then it's displayed normally.
+ `write(textToWrite)` set text to draw


TODO
====
Export project to maven.