package com.kosobudzki.customfontcanvas;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Utils class with helper methods for RxJava.
 *
 * @author krzysztof.kosobudzki
 */
public class RxJavaUtils {

    /**
     * Apply schedulers for RxJava call.
     *
     * For apply schedulers use compose().
     *
     * @param <T>
     * @return
     */
    public static final <T> Observable.Transformer<T, T> applySchedulers() {
        return new Observable.Transformer<T, T>() {
            @Override
            public Observable<? extends T> call(Observable<? extends T> observable) {
                return observable
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());
            }
        };
    }
}
